# VPN to proxy

This was made to be able to access websites from different country IP addresses, while keeping host OS connection intact.

## Build instructions

Build docker image.

```
docker build -t ludo444/vpn-to-proxy .
```

## Execute

To execute need to pass additional network privileges to container, path to OpenVPN config file and map host machine port.

```
docker run --name vpn-to-proxy --cap-add=NET_ADMIN --device=/dev/net/tun -v <path-to-openvpn-config-file>:/target-server.ovpn -p 127.0.0.1:8888:8888 ludo444/vpn-to-proxy
```

Optionally, good to pass explicit DNS address to container by `--dns <ip-address>`.

To connect set proxy in browser or application to `127.0.0.1:8888`.
