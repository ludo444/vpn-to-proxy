FROM alpine:3

RUN apk add --no-cache tinyproxy openvpn

COPY tinyproxy.conf /tinyproxy.conf

EXPOSE 8888

CMD tinyproxy -d -c /tinyproxy.conf & openvpn /target-server.ovpn
